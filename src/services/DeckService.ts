import {Deck} from "../model/Deck.ts";
import {SupabaseClient} from "@supabase/supabase-js";
import {Flashcard} from "../model/Flashcard.ts";
import UserService from "./UserService.ts";
import {Ref, ref, watch} from "vue";
import {Template} from "../model/Template.ts";

export default class DeckService {
  private supabase: SupabaseClient
  private userService: UserService;
  private decks = ref<Deck[]>([]);
  private cardsToStudy = ref<Flashcard[]>([]);

  constructor(supabase: SupabaseClient, userService: UserService) {
    this.supabase = supabase
    this.userService = userService
    watch(this.userService.getLoggedUser(), async (loggedUser) => {
      if (loggedUser != null) {
        this.list().then(decks => {
          this.decks.value = decks
          this.cardsToStudy.value = decks.map(it => it.cards).flat().filter(it => it.nextShowDate <= new Date())
        })
      } else {
        this.decks.value = []
        this.cardsToStudy.value = []
      }
    })
  }

  public get(id: number): Deck | undefined {
    return this.decks.value.find(it => it.id === id)
  }

  public async save(deck: Deck): Promise<Deck> {
    const {data, error} = await this.supabase
      .from("Deck")
      .update({
        title: deck.title, description: deck.description
      }).eq('id', deck.id)
      .select()
      .single()
    if (error) throw error
    this.list().then(decks => this.decks.value = decks);
    return data
  }

  public async create(deck: Deck): Promise<Deck> {
    const {data, error} = await this.supabase
      .from("Deck")
      .insert({
        title: deck.title, description: deck.description, owner: this.userService.getLoggedUser().value?.id
      }).select()
      .single()
    if (error) throw error
    deck.id = data.id
    this.decks.value = [...this.decks.value, deck]
    return deck
  }

  public async delete(deck: Deck): Promise<void> {
    const {error} = await this.supabase.from("Deck").delete().eq('id', deck.id)
    if (error) throw error
    this.decks.value = this.decks.value.filter(it => it.id != deck.id)
  }

  public async deleteCard(card: Flashcard): Promise<void> {
    const {error} = await this.supabase.from("Card").delete().eq('id', card.id)
    if (error) throw error
    this.decks.value = this.decks.value.map(it => it.id != card.deckId ? it : {...it, cards: it.cards.filter(it => it.id !== card.id)})
    this.cardsToStudy.value = [...this.cardsToStudy.value.filter(it => it.id !== card.id)]
  }

  public getCard(id: number): Flashcard | undefined {
    return this.decks.value.map(it => it.cards).flat().find(it => it.id === id)
  }

  public async saveCard(card: Flashcard): Promise<Flashcard> {
    const {data, error} = await this.supabase
      .from("Card")
      .insert(card)
      .select()
      .single()
    if (error) throw error
    this.cardsToStudy.value = [...this.cardsToStudy.value, data]
    this.decks.value = this.decks.value.map(it => it.id != card.deckId ? it : {...it, cards: [...it.cards, data]})
    return data
  }

  public async updateCard(card: Flashcard): Promise<void> {
    const {error} = await this.supabase
      .from("Card")
      .update(card)
      .eq('id', card.id)
    if (error) throw error
    if (card.nextShowDate <= new Date() && this.cardsToStudy.value.find(it => it.id === card.id) == null) {
      this.cardsToStudy.value = [...this.cardsToStudy.value, card]
    }
    this.decks.value = this.decks.value.map(it => it.id != card.deckId ? it : {...it, cards: [...it.cards.filter(that => that.id != card.id), card]})
  }

  public getCardsToStudy(): Ref<Flashcard[]> {
    return this.cardsToStudy
  }

  public getDecks(): Ref<Deck[]> {
    return this.decks
  }

  public async getTemplates(deckId: number): Promise<Template[]> {
    const {data, error} = await this.supabase
      .from('Template')
      .select('*')
      .eq('deckId', deckId)
    if (error) throw error
    return data
  }

  private async list(): Promise<Deck[]> {
    const {data, error} = await this.supabase
      .from('Deck')
      .select("id, title, description, Card(recto, verso, nextShowDate, aquisitionNumber, id, deckId)")
      .eq('owner', this.userService.getLoggedUser().value?.id)
    if (error) throw error
    return data.map(it => {
      const deck = new Deck(it.id, it.title, it.description)
      deck.cards = it.Card.map(card => {return {...card, nextShowDate: new Date(card.nextShowDate)}})
      return deck
    })
  }
}
