import {SupabaseClient, User} from "@supabase/supabase-js";
import {Ref, ref} from "vue";

export default class UserService {
  private supabase: SupabaseClient
  private loggedUser = ref<User | null>(null)

  constructor(supabase: SupabaseClient) {
    this.supabase = supabase
    supabase.auth.getUser().then(resp => {
      if (resp.data.user != null) {
        this.loggedUser.value = resp.data.user
      }
    })
  }

  public async login(email: string, password: string) {
    const {data, error} = await this.supabase.auth.signInWithPassword({
      email: email,
      password: password
    })
    if (error == null) {
      this.loggedUser.value = data.user
    }
    return this.loggedUser
  }

  public async logout() {
    await this.supabase.auth.signOut()
    this.loggedUser.value = null
  }

  public getLoggedUser(): Ref<User | null> {
    return this.loggedUser
  }

  async signup(name: string, email: string, password: string) {
    const {data, error} = await this.supabase.auth.signUp({
      email: email,
      password: password,
      options: {
        data: {
          first_name: name
        }
      }
    })
    if (error) throw error
    this.loggedUser.value = data.user
    return this.loggedUser
  }
}