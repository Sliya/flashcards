import {createApp} from 'vue'
import App from './App.vue'

import PrimeVue from 'primevue/config'
import 'primevue/resources/primevue.min.css'
import 'primevue/resources/themes/aura-light-green/theme.css'
import 'primeicons/primeicons.css'
import {createRouter, createWebHistory} from 'vue-router'

import Decks from './components/Decks.vue'
import Study from './components/Study.vue'
import Landing from './components/Landing.vue'
import Button from "primevue/button";
import MegaMenu from "primevue/megamenu";
import InputText from "primevue/inputtext";
import Password from "primevue/password";
import Divider from "primevue/divider";
import Card from "primevue/card";
import SplitButton from "primevue/splitbutton";
import ToastService from 'primevue/toastservice';
import Toast from "primevue/toast";
import Panel from "primevue/panel";
import FloatLabel from "primevue/floatlabel";
import DeckVue from "./components/DeckVue.vue";
import DeckEdit from "./components/DeckEdit.vue";
import CardAdd from "./components/CardAdd.vue";
import Textarea from "primevue/textarea";
import MultiSelect from "primevue/multiselect";
import ButtonGroup from "primevue/buttongroup";
import Badge from "primevue/badge";
import SpeedDial from "primevue/speeddial";
import Dropdown from "primevue/dropdown";

const routes = [
  {path: '/', component: Landing},
  {path: '/decks', component: Decks},
  {path: '/study', component: Study},
  {path: '/deck/:id', component: DeckVue},
  {path: '/deck/:deckId/card', component: CardAdd},
  {path: '/deck/:deckId/card/:id', component: CardAdd},
  {path: '/deck/:id/edit', component: DeckEdit},
  {path: '/deck/add', component: DeckEdit},
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

const app = createApp(App);
app.component('Button', Button);
app.component('ButtonGroup', ButtonGroup);
app.component('Divider', Divider);
app.component('MegaMenu', MegaMenu);
app.component('InputText', InputText);
app.component('TextArea', Textarea);
app.component('Password', Password);
app.component('Dropdown', Dropdown);
app.component('MultiSelect', MultiSelect);
app.component('SplitButton', SplitButton);
app.component('Card', Card);
app.component('Toast', Toast);
app.component('Panel', Panel);
app.component('Badge', Badge);
app.component('FloatLabel', FloatLabel);
app.component('SpeedDial', SpeedDial);
app.use(PrimeVue, {inputStyle: "filled"});
app.use(router)
app.use(ToastService)
app.mount('#app');

declare global { interface Array<T> { random(): T; } }

Array.prototype.random =  function () {
  if (this.length == 0) return undefined
  return this[Math.floor(Math.random() * this.length)]
}
