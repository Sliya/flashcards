import {Flashcard} from "./Flashcard.ts";

export class Deck {
  id?: number;
  title: string;
  description: string;
  cards: Flashcard[];

  constructor(id: number | undefined, title: string, description: string) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.cards = [];
  }
}