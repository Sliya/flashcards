export class Template {
  id?: number;
  deckId: number;
  recto: string;
  verso: string;
  title: string;

  constructor(id: number | undefined, deckId: number, recto: string, verso: string, title: string) {
    this.id = id;
    this.recto = recto;
    this.verso = verso;
    this.deckId = deckId;
    this.title = title;
  }
}