export class Flashcard {
  id?: number;
  deckId: number;
  recto: string;
  verso: string;
  nextShowDate: Date = new Date();
  aquisitionNumber: number = 0;

  constructor(id: number | undefined, deckId: number, recto: string, verso: string) {
    this.id = id;
    this.recto = recto;
    this.verso = verso;
    this.deckId = deckId;
  }
}